# Simple Maven App

This is a simple Maven Java app with two modules:

* `App.java` – the source code with a simple application.
* `AppTest.java` – unit tests for the main application.

The `pom.xml` file contains settings for Maven that can be used to build this app.

<!--Testing TeamCity Pull Requests feature for OAuth tokens AAAA -->

