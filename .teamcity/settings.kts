import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.CustomChart
import jetbrains.buildServer.configs.kotlin.CustomChart.*
import jetbrains.buildServer.configs.kotlin.amazonEC2CloudImage
import jetbrains.buildServer.configs.kotlin.amazonEC2CloudProfile
import jetbrains.buildServer.configs.kotlin.buildFeatures.perfmon
import jetbrains.buildServer.configs.kotlin.buildFeatures.pullRequests
import jetbrains.buildServer.configs.kotlin.buildSteps.maven
import jetbrains.buildServer.configs.kotlin.projectCustomChart
import jetbrains.buildServer.configs.kotlin.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2023.11"

project {

    buildType(Build)

    features {
        amazonEC2CloudImage {
            id = "PROJECT_EXT_34"
            profileId = "amazon-9"
            agentPoolId = "-2"
            name = "Cloud Image from the launch template and own AMI"
            keyPairName = "daria.krupkina"
            customizeLaunchTemplate = true
            launchTemplateCustomAmi = "ami-0817025aa39c203c6"
            source = LaunchTemplate(templateId = "lt-005cb4d2df991cc44", version = "3")
        }
        projectCustomChart {
            id = "PROJECT_EXT_37"
            title = "New chart title"
            seriesTitle = "Serie"
            format = CustomChart.Format.TEXT
            series = listOf(
                Serie(title = "Build Duration (all stages)", key = SeriesKey.BUILD_DURATION, sourceBuildTypeId = "SampleJavaAppMaven_2_Build")
            )
        }
        amazonEC2CloudProfile {
            id = "amazon-9"
            name = "Cloud AWS EC2 Profile"
            terminateIdleMinutes = 60
            region = AmazonEC2CloudProfile.Regions.EU_WEST_DUBLIN
            authType = accessKey {
                keyId = "zxx8cf38a04565c264f07ec1311dbd1579d06d5499a1cee7bfb"
                secretKey = "zxx74885e2dbb037873d73e681955d077d9fbc2401a885bca7246caa98c40d7bef2467b809870a2ad65775d03cbe80d301b"
            }
        }
    }
}

object Build : BuildType({
    name = "Build"

    artifactRules = "**/* => sources.zip"

    params {
        param("env.JDK_11_0", "%env.JRE_11_0%")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        maven {
            goals = "clean test"
            runnerArgs = "-Dmaven.test.failure.ignore=true"
            mavenVersion = bundled_3_8()
            jdkHome = "%env.JDK_11_0%"
        }
    }

    triggers {
        vcs {
        }
    }

    features {
        perfmon {
        }
        pullRequests {
            vcsRootExtId = "${DslContext.settingsRoot.id}"
            provider = gitlab {
                authType = storedToken {
                    tokenId = "tc_token_id:CID_10d77b39f7796f7123b9cadd5814ce04:2:fff701ee-dc85-4f73-ab4d-f516e3645c67"
                }
            }
        }
        pullRequests {
            provider = gitlab {
                authType = vcsRoot()
            }
        }
    }
})
